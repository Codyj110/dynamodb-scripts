import * as AWS from 'aws-sdk';

export class S3Repository {
    s3Client = () => new AWS.S3();

    constructor(
        private bucketName: string,
    ) {}

    async putObjectToS3(params): Promise<void | AWS.AWSError> {
        const response = await this.s3Client().putObject(params).promise();
        return response.$response.error;
    }

    async saveDataFile(path: string, fileName: string, fileContents: string): Promise<boolean> {
        path = path.replace(/\/$/, '');

        const params = {
            Body: fileContents,
            Bucket: this.bucketName,
            Key: path + '/' + fileName,
        };

        const response = await this.putObjectToS3(params);

        if (response == null) {
            return true;
        } else {
            throw new Error(response + ' Params: ' + JSON.stringify(params));
        }
    }
}