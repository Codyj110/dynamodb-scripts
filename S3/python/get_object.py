# read_s3.py
import boto3
BUCKET = 'MY_S3_BUCKET_NAME'
FILE_TO_READ = 'FOLDER_PATH/my_file.json'
client = boto3.client('s3',
                       aws_access_key_id='MY_AWS_KEY_ID',
                       aws_secret_access_key='MY_AWS_SECRET_ACCESS_KEY'
                     )
result = client.get_object(Bucket=BUCKET, Key=FILE_TO_READ) 

# read body of file
text = result["Body"].read().decode()