import * as AWS from 'aws-sdk';
import { DescribeStacksOutput } from 'aws-sdk/clients/cloudformation';


export class DescribeStack {
    cloudformation = () => new AWS.CloudFormation();

    async describe(): Promise<DescribeStacksOutput> {
        const params = {
            NextToken: null,
            StackName: 'some-stack-name',
        };

        return this.cloudformation().describeStacks(params).promise();
    }
}