import boto3

class SnsPublish:
    def __init__(self, topic_arn: str):
        self.topic_arn = topic_arn
        self.client = boto3.client('sns')


    def publish_to_topic(self, message: str, message_attributes: dict) -> None:
        self.client.publish(TopicArn=self.topic_arn,Message=message, MessageAttributes=message_attributes)



topic_arn = ""
message = {}
attributes = {
        'action': {
            'DataType': 'String',
            'StringValue': 'some-action-value',
        },
	'entity': {
		'DataType': 'String',
            'StringValue': 'some-entity-name',
		}
    }


sns_publish = SnsPublish(topic_arn)
sns_publish.publish_to_topic(json.dumps(message), attributes)