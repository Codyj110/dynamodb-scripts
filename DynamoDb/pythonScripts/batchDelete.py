import boto3

dynamodb = boto3.resource('dynamodb', 'us-east-2')
table = dynamodb.Table('some_table')

scan = table.scan(
    ProjectionExpression='#k',
    ExpressionAttributeNames={
        '#k': 'some_attribute_column'
    }
)
with table.batch_writer() as batch:
    for each in scan['Items']:
        deleted_records += 1
        batch.delete_item(Key=each)