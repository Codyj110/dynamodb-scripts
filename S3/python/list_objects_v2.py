import boto3

# Get the service client
s3 = boto3.client('s3')

"""
If you don't have CLI and therefore AWS Credentials setup , then un-comment the lines below and remove the line above. Add your AWS credentials.

s3 = boto3.client(
   's3',
   aws_access_key_id='AKIAIO5FODNN7EXAMPLE',
   aws_secret_access_key='ABCDEF+c2L7yXeGvUyrPgYsDnWRRC1AYEXAMPLE'
) 
"""

bucket_name = "some_bucket_name"
prefix = "test/upload/"

# Upload tmp.txt to bucket-name at key-name
try:
    kwargs = {"Prefix": prefix, "Bucket": bucket_name}

    object_list = []

    # loop until there are no more next tokens and append to our return list
    while True:
        response = s3.list_objects_v2(**kwargs)  # type: dict

        contents = response.get('Contents')

        if contents is None:
            break
        else:
            for item in contents:
                object_list.append(item['Key'])

        next_token = response.get('NextContinuationToken')

        if next_token is None:
            break
        else:
            kwargs['ContinuationToken'] = next_token

except Exception as e:
    print(e)

