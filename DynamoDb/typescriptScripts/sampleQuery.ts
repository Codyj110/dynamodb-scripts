import { DynamoDB } from 'aws-sdk';

const client = new DynamoDB();

async queryDynamo(firstname: string, lastName: string): Promise<any> => {
      const params = {
        TableName: dynamodbTableName,
        IndexName: 'secondIndexName',
        KeyConditionExpression: '#placeHolderAttrVarOne = :placeHolderVarOne and begins_with(#placeHolderAttrVarTwo, :email)',
        ExpressionAttributeNames: {
          '#placeHolderAttrVarOne': 'dynamoAttributeNameOne',
          '#placeHolderAttrVarTwo': 'dynamoAttributeNameTwo',
        },
        ExpressionAttributeValues: {
          ':placeHolderVarOne': {'S': firstname},
          ':placeHolderVarTwo': {'S': lastName},
        },
      };
      return await (<any> client).query(params).promise();
  }