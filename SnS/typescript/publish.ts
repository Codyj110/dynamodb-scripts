const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-2' });

const sns = new AWS.SNS();




const params = {
  Message: 'Some data', /* required */
  MessageAttributes: {
    'action': {
      DataType: 'String', /* required */
      StringValue: 'someActionValue'
    },
    'entity': {
      DataType: 'String', /* required */
      StringValue: 'someEntityName'
    },
  },
  TopicArn: 'someTopicArn'
};

sns.publish(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log(data);           // successful response
});