import boto3
import json


class DynamoBatchWrite:

    def __init__(self, table_name: str,item_mapping_func: callable ,batch_size: int=50):
        # create client to batch write with
        dynamodb = boto3.resource('dynamodb')
        self.__table = dynamodb.Table(table_name)
        # set batchsize for our batchwrite
        self.__batch_size = batch_size
        # set the mapping function that will be used to map your items
        # to the needed format for the dynamo table
        # signature expected my_map(item: dict) -> dict:
        self.__item_mapping_func = item_mapping_func

    def __split_list_into_lists(self, list_to_split: list) -> list:
        """splits list into given batch size"""
        new_list = []
        # For item index in a range that is a length of list_to_split,
        for index in range(0, len(list_to_split), self.__batch_size):
            # Create an index range for list_to_split of max_array_size items:
            new_list.append(list_to_split[index:index + self.__batch_size])
        return new_list

    def __apply_map(self, items: list):
        """"""
        new_list = []
        for item in items:
            new_item = self.__item_mapping_func(item)
            new_list.append(new_item)
        return new_list

    def writeItems(self, items: list) -> None:
        # apply map to given items
        mapped_items = self.__apply_map(items)
        # split items into correct batch size
        batched_items = self.__split_list_into_lists(mapped_items)
        # call batch write for each batched list
        for single_batch in batched_items:
            self.__dynamoBatchWrite(single_batch)

    def writeItemsFromJsonFile(self, file_path: str) -> None:
        with open(file_path) as file:
            raw_data = json.load(file)
        self.writeItems(raw_data)

    def __dynamoBatchWrite(self, items: list) -> None:
        """batch writes its for given list"""
        with self.__table.batch_writer() as batch:
            for item in range(len(items)):
                batch.put_item(
                    Item=items[item]
                )

