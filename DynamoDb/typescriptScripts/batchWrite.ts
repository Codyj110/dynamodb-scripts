import { DynamoDB } from 'aws-sdk';
const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-2' });

export class DynamoBatchWrite {
    private dynamodbClient: DynamoDB.DocumentClient;.;

    constructor(private tableName: string,
                private itemMapper: Function,
                private batchSize: number = 25,
                region: string = 'us-east-2') {

        this.dynamodbClient = new AWS.DynamoDB.DocumentClient();
    }

    private splitArrayIntoArrays(items: any[]):  any[][] {
        const batchOfItems = [];
        for (let index = 0; index < items.length; index += this.batchSize) {
            const newBatch = items.slice(index, index + this.batchSize);
            batchOfItems.push(newBatch);
        }
        return batchOfItems;
    }

    private createParams(items: any[]): any {

        const wrappedItems = items.map((item: any[]): any => {
            return {
                PutRequest: { Item: item },
            };
        });

        return {
            RequestItems: {
                [this.tableName]: wrappedItems,
            },
        };
    }

    private applyMap(items: any[]): any[] {
        return items.map((item: any): any => {
            return this.itemMapper(item);
        });
    }

    writeItems(items: any[]): void {
        // apply map
        const mappedItems = this.applyMap(items);
        const batchedItems = this.splitArrayIntoArrays(mappedItems);
        batchedItems.forEach((batch: any[]): void => {
            const params = this.createParams(batch);
            console.log(JSON.stringify(params));
            this.dynamodbClient.batchWrite(params, (err, data) => {
                if (err) { console.log(err); } else { console.log(data); } // successful response
            });
        });
    }
}
